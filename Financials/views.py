from django.http.response import HttpResponse
from django.shortcuts import render

# Create your views here.

from django.views.generic import View
from BusinessLogic.ProcessFees import ProcessFees

class FinancialView(View):
    def get(self, request):
        historyStubs = [{
            'studentName': 'Dhruv',
            'course': 'Japanese',
            'feePaid': 2500
        },{
            'studentName': 'Ullas Kashyap',
            'course': 'Cinematography',
            'feePaid': 5000
        },{
            'studentName': 'HP',
            'course': 'Wizadry',
            'feePaid': 20000
        },{
            'studentName': 'Yazhini',
            'course': 'Carpentry',
            'feePaid': 100
        }]
        courses = [{
            'courseName': 'Japanese'
        },{
            'courseName': 'Cinematography'
        },{
            'courseName': 'Wizadry'
        },{
            'courseName': 'Carpentry'
        }]
        if "userId" in request.GET:
            userId = request.GET['userId']
            print userId
        return render(request, template_name="index.html", context={"historyStubs":historyStubs, "courses":courses})

    def post(self, request):
        fees = request.POST["fees"]
        FeesProcessor = ProcessFees()
        status = FeesProcessor.storePayment(fees=fees, user="adivandhya")
        if status == True:
            return HttpResponse("Successful")
        elif status == False:
            return HttpResponse("Unsuccessful")