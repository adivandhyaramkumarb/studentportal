angular.module('FinancialModule',[])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    })
    .controller('FinancialController',['$scope',function($scope){
        $scope.favoriteFruit = 'apple';
    }]);
