from django.conf.urls import url, include
from django.contrib import admin

from Financials import views

urlpatterns = [
    url(r'^$', views.FinancialView.as_view(), name="Financial View")
]
