from django.conf.urls import url, include
from django.contrib import admin
from Course import views

urlpatterns = [
	url(r'^$', views.Courses.as_view(), name = 'CoursesView'), 
	url(r'^Maths$', views.CourseDetails.as_view(), name = 'CourseDetailView'),
]
