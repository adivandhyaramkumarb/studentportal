from django.shortcuts import render
from django.template.context_processors import request

from django.views.generic import View


# Create your views here.
class Courses(View):
	
	def get(self,request):
		data = [{'CourseID':'1','CourseName' : 'Coursename'}]
		#json_data = json.dumps(data)
		return render(request, template_name='index.html',context = {'courses':data}) 

class CourseDetails(View):
	def get(self,request):
		data = [{'CourseDescription':'Course Description','Faculty' : 'Faculty','CourseCredit':'Course Credit'}]
		return render(request, template_name='coursedetails.html',context = {'coursedetails':data})
