from django.shortcuts import render
from django.http.response import HttpResponse

# Create your views here.
from django.views import View


class HomeView(View):
    def get(self, request):
        print "test"
        return render(request, template_name="Home/index.html")

    def post(self, request):
        customMessage = request.POST['customMessage']
        return render(request,context={'Message':customMessage}, template_name="Home/newPage.html")